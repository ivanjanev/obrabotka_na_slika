/**
 * Created by ivanj on 22-Dec-16.
 */

//preview image

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgPreview').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//send data to preview image && download script
$(document).ready(function () {
    // on loading of the page, set text for no image
    if($('#imgPreview').attr('src') == '#'){
        $('.no_img_error').removeClass('hidden');
        $('.previewDiv img').addClass('hidden');

        // disable download button until an image is uploaded
        $('#downloadBtn').addClass('disabled');
        $('#downloadBtn').prop('disabled',true);
    }

    //on upload preview image && set download button to enable
    $('#imgUpload').change(function () {
        $('.no_img_error').addClass('hidden');
        $('.previewDiv img').removeClass('hidden');

        var file= this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;


        readURL(this);

        $('#downloadBtn').removeClass('disabled');
        $('#downloadBtn').prop('disabled',false);

        $('.radio_button').prop('checked',false);
    });



    $('#downloadBtn').click(function () {
            var fileURL=$("#imgPreview").attr('src');


            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'imgCanvas.jpg';
            document.body.appendChild(a);
            a.click();
    });

});

//send img to backend for reconstruction
$('.radio_button').change(function(){
	if(this.value != "channels"){
		$('#submit_filter').trigger('click');
		$('.collapse').collapse('hide');
	}
});
$(".uploadDiv form").submit(function (event) {

    event.preventDefault();

    loading=function () {
        $("#imgPreview").attr("src","img/hourglass.svg");
    };

    var formData = new FormData($(this)[0]);

    $.ajax({
        url: "upload.php",
        type: "POST",
        data: formData,
        async: true,
        success: function (imgSource) {

            try {
                var response = jQuery.parseJSON(imgSource);
                console.log(imgSource);
                $.each(response,function (key,value) {
                    if(key===0)
                    {
                        $("#imgPreview").attr("src",value);
                    }
                    else
                    {
						$("#"+key).attr('title', value);
                        $("#"+key).css('background',value);
                        $("#"+key).css('min-height','50px');
                    }
                });
            }
            catch (e) {
                $("#imgPreview").attr("src",imgSource);
            }

        },
        cache: false,
        contentType: false,
        processData: false
    });

    loading();

}); 




